Toolbox plugin for Confluence

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/CONFBOX>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
