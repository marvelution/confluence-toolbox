/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.param.ParameterAssistant;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.Macro.BodyType;
import com.atlassian.confluence.macro.Macro.OutputType;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.google.common.collect.Maps;
import com.google.gson.JsonObject;
import com.marvelution.confluence.plugins.toolbox.macros.MavenArtifactInfoMacro.JsonObjectCompletionHandler;
import com.marvelution.confluence.plugins.toolbox.macros.maven.ArtifactVersion;
import com.marvelution.confluence.plugins.toolbox.utils.ResponseAnswer;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpProvider;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Request;

/**
 * TestCase for {@link SpaceKeyMacro}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class MavenArtifactInfoMacroTest {

	private static final String SPACE_KEY = "TOOLBOX";

	@Mock private MacroAssistant macroAssistant;
	@Mock private XhtmlContent xhtmlContent;
	@Mock private EnvironmentAssistant environmentAssistant;
	@Mock private ParameterAssistant parameterAssistant;
	@Mock private ConversionContext conversionContext;
	@Mock private LinkAssistant linkAssistant;
	@Mock private VelocityHelperService velocityHelperService;
	@Mock private AsyncHttpProvider asyncHttpProvider;

	private MavenArtifactInfoMacro macro;
	private Space space;
	private Page page;

	/**
	 * Setup the test mocks
	 */
	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		space = new Space(SPACE_KEY);
		page = new Page();
		page.setSpace(space);
		when(macroAssistant.getEnvironmentAssistant()).thenReturn(environmentAssistant);
		when(macroAssistant.getParameterAssistant()).thenReturn(parameterAssistant);
		when(macroAssistant.getXhtmlContent()).thenReturn(xhtmlContent);
		when(conversionContext.getEntity()).thenReturn(page);
		when(asyncHttpProvider.execute(isA(Request.class), isA(JsonObjectCompletionHandler.class)))
				.thenAnswer(new ResponseAnswer<ListenableFuture<JsonObject>>() {
			@Override
			public ListenableFuture<JsonObject> answer(InvocationOnMock invocation) throws Throwable {
				AsyncCompletionHandler<JsonObject> handler = (AsyncCompletionHandler<JsonObject>) invocation
					.getArguments()[1];
				JsonObject json = handler.onCompleted(getJsonResponseFromRequest((Request) invocation
					.getArguments()[0]));
				return getListenableFuture(json, JsonObject.class);
			}
		});
		AuthenticatedUserThreadLocal.setUser(null);
		macro = new MavenArtifactInfoMacro(macroAssistant, linkAssistant, velocityHelperService) {

			/**
			 * {@inheritDoc}
			 */
			@Override
			AsyncHttpClient getAsyncHttpClient() {
				return new AsyncHttpClient(asyncHttpProvider);
			}

		};
	}

	/**
	 * Test {@link MavenArtifactInfoMacro#populateContext(Map, MacroInfo)} with a classifier set
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testPopulateWithClassifier() throws Exception {
		Map<String, String> macroParams = Maps.newHashMap();
		macroParams.put("groupId", "com.marvelution.jira.plugins");
		macroParams.put("artifactId", "jira-hudson");
		macroParams.put("classifier", "distribution");
		MacroInfo macroInfo = new MacroInfo(macroParams, "", conversionContext, macroAssistant);
		Map<String, Object> context = Maps.newHashMap();
		macro.populateContext(context, macroInfo);
		List<ArtifactVersion> versions = (List<ArtifactVersion>) context.get("versions");
		assertThat(versions.size(), is(18));
	}

	/**
	 * Test {@link MavenArtifactInfoMacro#populateContext(Map, MacroInfo)} width empty search results
	 */
	@Test
	public void testPopulateNoSearchResults() throws Exception {
		Map<String, String> macroParams = Maps.newHashMap();
		macroParams.put("groupId", "com.marvelution.jira.plugins");
		macroParams.put("artifactId", "jira-hudson");
		macroParams.put("classifier", "distro");
		MacroInfo macroInfo = new MacroInfo(macroParams, "", conversionContext, macroAssistant);
		Map<String, Object> context = Maps.newHashMap();
		try {
			macro.populateContext(context, macroInfo);
			fail("This test should fail! No search results to list");
		} catch (MacroExecutionException e) {
			assertThat(e.getMessage(), is("No Maven artifacts found to display."));
		}
	}

	/**
	 * Test {@link MavenArtifactInfoMacro#populateContext(Map, MacroInfo)} with no repo
	 */
	@Test
	public void testPopulateNoRepoWithId() throws Exception {
		Map<String, String> macroParams = Maps.newHashMap();
		macroParams.put("repo", "test");
		macroParams.put("groupId", "com.marvelution.jira.plugins");
		macroParams.put("artifactId", "jira-hudson");
		macroParams.put("classifier", "distribution");
		MacroInfo macroInfo = new MacroInfo(macroParams, "", conversionContext, macroAssistant);
		Map<String, Object> context = Maps.newHashMap();
		try {
			macro.populateContext(context, macroInfo);
			fail("This test should fail! No search results to list");
		} catch (MacroExecutionException e) {
			assertThat(e.getMessage(), is("No Repository with Id test"));
		}
	}

	/**
	 * Test {@link MavenArtifactInfoMacro#populateContext(Map, MacroInfo)}
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testPopulate() throws Exception {
		Map<String, String> macroParams = Maps.newHashMap();
		macroParams.put("groupId", "com.marvelution.jira.plugins");
		macroParams.put("artifactId", "jira-sonar-plugin");
		MacroInfo macroInfo = new MacroInfo(macroParams, "", conversionContext, macroAssistant);
		Map<String, Object> context = Maps.newHashMap();
		macro.populateContext(context, macroInfo);
		List<ArtifactVersion> versions = (List<ArtifactVersion>) context.get("versions");
		assertThat(versions.size(), is(9));
	}

	/**
	 * Test {@link Macro#getBodyType()}
	 */
	@Test
	public void testGetBodyType() {
		assertThat(macro.getBodyType(), is(BodyType.NONE));
	}

	/**
	 * Test {@link Macro#getOutputType()}
	 */
	@Test
	public void testGetOutputType() {
		assertThat(macro.getOutputType(), is(OutputType.BLOCK));
	}

}
