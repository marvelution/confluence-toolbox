/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.param.ParameterAssistant;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.Macro.BodyType;
import com.atlassian.confluence.macro.Macro.OutputType;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.google.common.collect.Maps;

/**
 * TestCase for {@link SpaceKeyMacro}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class LinkToMacroTest {

	private static final String SPACE_KEY = "TOOLBOX";

	@Mock private MacroAssistant macroAssistant;
	@Mock private XhtmlContent xhtmlContent;
	@Mock private EnvironmentAssistant environmentAssistant;
	@Mock private ParameterAssistant parameterAssistant;
	@Mock private ConversionContext conversionContext;
	@Mock private LinkAssistant linkAssistant;

	private Macro macro;
	private Space space;
	private Page page;

	/**
	 * Setup the test mocks
	 */
	@Before
	public void setUp() {
		space = new Space(SPACE_KEY);
		page = new Page();
		page.setSpace(space);
		when(macroAssistant.getEnvironmentAssistant()).thenReturn(environmentAssistant);
		when(macroAssistant.getParameterAssistant()).thenReturn(parameterAssistant);
		when(macroAssistant.getXhtmlContent()).thenReturn(xhtmlContent);
		when(conversionContext.getEntity()).thenReturn(page);
		when(linkAssistant.getContextPath()).thenReturn("");
		macro = new LinkToMacro(macroAssistant, linkAssistant);
	}

	/**
	 * Test {@link Macro#execute(java.util.Map, String, ConversionContext)}
	 */
	@Test
	public void testExecuteNoParameters() {
		try {
			assertThat(macro.execute(new HashMap<String, String>(), "", conversionContext), is(SPACE_KEY));
			fail("This test should fail since the target parameter is required");
		} catch (MacroExecutionException e) {
			assertThat(e.getMessage(), is("Please supply the target."));
		}
	}

	/**
	 * Test {@link Macro#execute(java.util.Map, String, ConversionContext)}
	 */
	@Test
	public void testExecute() {
		try {
			Map<String, String> params = Maps.newHashMap();
			params.put("0", "dashboard");
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/dashboard.action\">dashboard</a>"));
			params.put("0", "admin");
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/admin/console.action\">admin</a>"));
			params.put("0", "logout");
			assertThat(macro.execute(params, "", conversionContext), is("<a href=\"/logout.action\">logout</a>"));
			params.put("0", "login");
			assertThat(macro.execute(params, "", conversionContext), is("<a href=\"/login.action\">login</a>"));
			params.put("0", "signup");
			assertThat(macro.execute(params, "", conversionContext), is("<a href=\"/signup.action\">signup</a>"));
			params.put("0", "news");
			assertThat(macro.execute(params, "", conversionContext), is("<a href=\"/display/blog-TOOLBOX\">news</a>"));
			params.put("0", "space admin");
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/spaces/spaceadmin.action?key=TOOLBOX\">space admin</a>"));
			params.put("0", "space admin");
			params.put("space", "OTHERSPACE");
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/spaces/spaceadmin.action?key=OTHERSPACE\">space admin</a>"));
			params.put("0", "news");
			params.put("text", "Link Text");
			params.put("space", "OTHERSPACE");
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/display/blog-OTHERSPACE\">Link Text</a>"));
		} catch (MacroExecutionException e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test {@link Macro#getBodyType()}
	 */
	@Test
	public void testGetBodyType() {
		assertThat(macro.getBodyType(), is(BodyType.NONE));
	}

	/**
	 * Test {@link Macro#getOutputType()}
	 */
	@Test
	public void testGetOutputType() {
		assertThat(macro.getOutputType(), is(OutputType.INLINE));
	}

}
