/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.events;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.runners.MockitoJUnitRunner;

import twitter4j.Twitter;
import twitter4j.TwitterException;

import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.TinyUrl;
import com.atlassian.event.api.EventPublisher;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccount;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccountManager;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterClientFactory;

/**
 * Test case for the {@link BlogPostCreateEventTweeter}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class BlogPostCreateEventTweeterTest {

	@Mock private EventPublisher eventPublisher;
	@Mock private TwitterClientFactory clientFactory;
	@Mock private TwitterAccountManager accountService;
	@Mock private TwitterAccount twitterAccount;
	@Mock private Twitter twitter;
	@Mock private BlogPost blogPost;

	private BlogPostCreateEventTweeter eventListener;

	/**
	 * Setup the {@link BlogPostCreateEventTweeter} for the tests
	 */
	@Before
	public void setUp() {
		eventListener = new BlogPostCreateEventTweeter(eventPublisher, clientFactory, accountService) {
			@Override
			protected String getTinyUrl(BlogPost post) {
				TinyUrl tinyUrl = new TinyUrl(blogPost);
				return "http://localhost:8090/x/" + tinyUrl.getIdentifier();
			}
		};
	}

	/**
	 * Test the {@link BlogPostCreateEventTweeter#tweetBlogPost(BlogPostEvent)} with no configured {@link TwitterAccount}s
	 */
	@Test
	public void testTweetBlogPostNoAccounts() {
		when(blogPost.getCreatorName()).thenReturn("markrekveld");
		when(accountService.find("markrekveld")).thenReturn(new TwitterAccount[0]);
		BlogPostCreateEvent event = new BlogPostCreateEvent("Test Source", blogPost);
		eventListener.tweetBlogPost(event);
		verify(accountService).find("markrekveld");
		verify(blogPost).getCreatorName();
		verify(blogPost, VerificationModeFactory.times(0)).getTitle();
	}

	/**
	 * Test the {@link BlogPostCreateEventTweeter#tweetBlogPost(BlogPostEvent)} with a configured {@link TwitterAccount} but disabled auto
	 * tweeter
	 */
	@Test
	public void testTweetBlogPostWithAccountButDisabledAutoTweeter() {
		when(blogPost.getCreatorName()).thenReturn("markrekveld");
		when(blogPost.getTitle()).thenReturn("My Great Blog Post");
		when(accountService.find("markrekveld")).thenReturn(new TwitterAccount[] {twitterAccount});
		when(twitterAccount.isAutoTweetBlogPosts()).thenReturn(false);
		BlogPostCreateEvent event = new BlogPostCreateEvent("Test Source", blogPost);
		eventListener.tweetBlogPost(event);
		verify(accountService).find("markrekveld");
		verify(twitterAccount).isAutoTweetBlogPosts();
		verify(blogPost).getCreatorName();
		verify(blogPost, VerificationModeFactory.times(0)).getTitle();
	}

	/**
	 * Test the {@link BlogPostCreateEventTweeter#tweetBlogPost(BlogPostEvent)} with a configured {@link TwitterAccount}
	 */
	@Test
	public void testTweetBlogPostSmallBlogTitle() {
		try {
			when(blogPost.getId()).thenReturn(10000L);
			when(blogPost.getCreatorName()).thenReturn("markrekveld");
			when(blogPost.getTitle()).thenReturn("My Great Blog Post");
			when(accountService.find("markrekveld")).thenReturn(new TwitterAccount[] {twitterAccount});
			when(twitterAccount.isAutoTweetBlogPosts()).thenReturn(true);
			when(clientFactory.create(twitterAccount)).thenReturn(twitter);
			BlogPostCreateEvent event = new BlogPostCreateEvent("Test Source", blogPost);
			eventListener.tweetBlogPost(event);
			verify(twitter).updateStatus("My Great Blog Post http://localhost:8090/x/ECc");
			verify(accountService).find("markrekveld");
			verify(twitterAccount).isAutoTweetBlogPosts();
			verify(blogPost).getCreatorName();
			verify(blogPost).getTitle();
			verify(blogPost).getId();
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test the {@link BlogPostCreateEventTweeter#tweetBlogPost(BlogPostEvent)} with a configured {@link TwitterAccount}
	 * and a long blog post title
	 */
	@Test
	public void testTweetBlogPostLongBlogTitle() {
		try {
			when(blogPost.getId()).thenReturn(10000L);
			when(blogPost.getCreatorName()).thenReturn("markrekveld");
			when(blogPost.getTitle()).thenReturn("My very long great Blog Post title - read all about all kinds of stuff that make my world go rond and rond all day long");
			when(accountService.find("markrekveld")).thenReturn(new TwitterAccount[] {twitterAccount});
			when(twitterAccount.isAutoTweetBlogPosts()).thenReturn(true);
			when(clientFactory.create(twitterAccount)).thenReturn(twitter);
			BlogPostCreateEvent event = new BlogPostCreateEvent("Test Source", blogPost);
			eventListener.tweetBlogPost(event);
			verify(twitter).updateStatus("My very long great Blog Post title - read all about all kinds of stuff that make my world go rond and rond al... http://localhost:8090/x/ECc");
			verify(accountService).find("markrekveld");
			verify(twitterAccount).isAutoTweetBlogPosts();
			verify(blogPost).getCreatorName();
			verify(blogPost).getTitle();
			verify(blogPost).getId();
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test the {@link BlogPostCreateEventTweeter#tweetBlogPost(BlogPostEvent)} with a configured {@link TwitterAccount}
	 * But {@link Twitter#updateStatus(String)} throwing an exception
	 */
	@Test
	public void testTweetBlogPostUpdateStatusException() {
		try {
			when(blogPost.getId()).thenReturn(10000L);
			when(blogPost.getCreatorName()).thenReturn("markrekveld");
			when(blogPost.getTitle()).thenReturn("My Great Blog Post");
			when(accountService.find("markrekveld")).thenReturn(new TwitterAccount[] {twitterAccount});
			when(twitterAccount.isAutoTweetBlogPosts()).thenReturn(true);
			when(clientFactory.create(twitterAccount)).thenReturn(twitter);
			when(twitter.updateStatus(anyString())).thenThrow(new TwitterException("Error"));
			BlogPostCreateEvent event = new BlogPostCreateEvent("Test Source", blogPost);
			eventListener.tweetBlogPost(event);
			verify(twitter).updateStatus("My Great Blog Post http://localhost:8090/x/ECc");
			verify(accountService).find("markrekveld");
			verify(twitterAccount).isAutoTweetBlogPosts();
			verify(blogPost).getCreatorName();
			verify(blogPost).getTitle();
			verify(blogPost).getId();
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
