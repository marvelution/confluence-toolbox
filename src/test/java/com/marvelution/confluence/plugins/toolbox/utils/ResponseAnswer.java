/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.utils;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.codec.digest.DigestUtils;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import com.marvelution.confluence.plugins.toolbox.macros.MavenArtifactInfoMacro;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Request;
import com.ning.http.client.Response;

/**
 * Abstract {@link Response} {@link Answer} implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public abstract class ResponseAnswer<T> implements Answer<T> {

	/**
	 * Get a {@link Response} {@link Mock} from the given {@link Request}
	 * 
	 * @param request the {@link Request} to get he {@link Response} for
	 * @return the {@link Response} {@link Mock}
	 * @throws Exception in case the Response JSON resource could not be loaded
	 */
	protected final Response getJsonResponseFromRequest(Request request) throws Exception {
		Response response = mock(Response.class);
		when(response.getResponseBodyAsStream()).thenReturn(MavenArtifactInfoMacro.class
			.getResourceAsStream("./maven/" + DigestUtils.md5Hex(request.getUrl()) + ".json"));
		return response;
	}

	/**
	 * Get a {@link ListenableFuture} for the given {@link Response} and type
	 * 
	 * @param response the {@link Response} that should be wrapped by the {@link ListenableFuture}
	 * @param type the Type of the Response
	 * @return the {@link ListenableFuture} {@link Mock}
	 * @throws Exception in case of errors
	 */
	@SuppressWarnings("unchecked")
	protected <V> ListenableFuture<V> getListenableFuture(V response, Class<V> type) throws Exception {
		ListenableFuture<V> future = (ListenableFuture<V>) mock(ListenableFuture.class);
		when(future.get()).thenReturn(response);
		return future;
	}

}
