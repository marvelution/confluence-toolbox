/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.param.ParameterAssistant;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.PageResourceIdentifier;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.Macro.BodyType;
import com.atlassian.confluence.macro.Macro.OutputType;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.xhtml.api.Link;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.google.common.collect.Maps;
import com.marvelution.confluence.plugins.toolbox.macros.DirectoryLinkMacro.Type;

/**
 * TestCase for {@link SpaceKeyMacro}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class DirectoryLinkMacroTest {

	private static final String SPACE_KEY = "TOOLBOX";
	private static final String SPACE_NAME = "Confluence Toolbox";

	@Mock private MacroAssistant macroAssistant;
	@Mock private XhtmlContent xhtmlContent;
	@Mock private EnvironmentAssistant environmentAssistant;
	@Mock private ParameterAssistant parameterAssistant;
	@Mock private ConversionContext conversionContext;

	private Macro macro;
	private Space space;
	private Page page;

	/**
	 * Setup the test mocks
	 */
	@Before
	public void setUp() {
		space = new Space(SPACE_KEY);
		space.setName(SPACE_NAME);
		page = new Page();
		page.setSpace(space);
		when(macroAssistant.getEnvironmentAssistant()).thenReturn(environmentAssistant);
		when(macroAssistant.getParameterAssistant()).thenReturn(parameterAssistant);
		when(macroAssistant.getXhtmlContent()).thenReturn(xhtmlContent);
		when(conversionContext.getEntity()).thenReturn(page);
		macro = new DirectoryLinkMacro(macroAssistant);
	}

	/**
	 * Test {@link Macro#execute(java.util.Map, String, ConversionContext)}
	 */
	@Test
	public void testExecuteNoParameters() {
		try {
			when(xhtmlContent.convertLinkToView(isA(Link.class), eq(conversionContext)))
				.thenAnswer(new Answer<String>() {
					@Override
					public String answer(InvocationOnMock invocation) throws Throwable {
						Link link = (Link) invocation.getArguments()[0];
						PageResourceIdentifier identifier = (PageResourceIdentifier) link
							.getDestinationResourceIdentifier();
						Space space = new Space(identifier.getSpaceKey());
						Page page = new Page();
						page.setSpace(space);
						page.setTitle(identifier.getTitle());
						return "<a href=\"" + GeneralUtil.getPageUrl(page) + "/\">" + link.getBody().getBody() + "</a>";
					}
			});
			Map<String, String> params = Maps.newHashMap();
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/display/ALLDOCS/Confluence+Toolbox/\">" + space.getName() + "</a>"));
			params.put("type", Type.DOCUMENTATION.name());
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/display/ALLDOCS/Confluence+Toolbox/\">" + space.getName() + "</a>"));
			params.put("type", Type.REPOSITORY.name());
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/display/REPO/Confluence+Toolbox/\">" + space.getName() + "</a>"));
			params.put("type", Type.REPOSITORY.name());
			params.put("text", "Other Text");
			assertThat(macro.execute(params, "", conversionContext),
				is("<a href=\"/display/REPO/Confluence+Toolbox/\">Other Text</a>"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test {@link Macro#execute(java.util.Map, String, ConversionContext)}
	 */
	@Test
	public void testExecuteXhtmlException() {
		try {
			when(xhtmlContent.convertLinkToView(isA(Link.class), eq(conversionContext)))
				.thenThrow(new XhtmlException("Mocked Exception"));
			Map<String, String> params = Maps.newHashMap();
			params.put("type", DirectoryLinkMacro.Type.REPOSITORY.name());
			params.put("text", "Other Text");
			assertThat(macro.execute(params, "", conversionContext), is("Should not get to here!"));
			fail("");
		} catch (Exception e) {
			assertThat(e.getMessage(), is("Mocked Exception"));
		}
	}

	/**
	 * Test {@link Macro#getBodyType()}
	 */
	@Test
	public void testGetBodyType() {
		assertThat(macro.getBodyType(), is(BodyType.NONE));
	}

	/**
	 * Test {@link Macro#getOutputType()}
	 */
	@Test
	public void testGetOutputType() {
		assertThat(macro.getOutputType(), is(OutputType.INLINE));
	}

}
