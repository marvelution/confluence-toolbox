/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.twitter;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Default {@link TwitterClientFactory} implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class TwitterClientFactoryService implements TwitterClientFactory {

	private static final Logger LOGGER = Logger.getLogger(TwitterClientFactoryService.class);
	private static final String CONSUMER_KEY = "XkivxUutTxjRdLwnD2IXDg";
	private static final String CONSUMER_SECRET = "BtohzWuc1aqLLbFf7XD4iYn4JfPRzEszHuJndBQ1wQ";
	private TwitterFactory twitterFactory;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Twitter create() {
		return getTwitterFactory().getInstance();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Twitter create(TwitterAccount account) {
		Preconditions.checkNotNull(account);
		Twitter twitter = create();
		twitter.setOAuthAccessToken(new AccessToken(account.getToken(), account.getTokenSecret()));
		return twitter;
	}

	/**
	 * Getter for the {@link TwitterFactory}
	 * 
	 * @return the {@link TwitterFactory}
	 */
	private TwitterFactory getTwitterFactory() {
		if (twitterFactory == null) {
			ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
			configurationBuilder.setDebugEnabled(LOGGER.isDebugEnabled());
			configurationBuilder.setOAuthConsumerKey(CONSUMER_KEY);
			configurationBuilder.setOAuthConsumerSecret(CONSUMER_SECRET);
			twitterFactory = new TwitterFactory(configurationBuilder.build());
		}
		return twitterFactory;
	}

}
