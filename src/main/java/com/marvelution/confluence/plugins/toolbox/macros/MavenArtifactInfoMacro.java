/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.randombits.storage.IndexedStorage;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.marvelution.confluence.plugins.toolbox.macros.maven.ArtifactVersion;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

/**
 * Maven Artifact Information Macro
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class MavenArtifactInfoMacro extends BaseVelocityMacro {

	private static final Logger LOGGER = Logger.getLogger(MavenArtifactInfoMacro.class);

	private static final String REPO_URL_PARAM = "repoUrl";
	private static final String REPO_PARAM = "repo";
	private static final String GROUPID_PARAM = "groupId";
	private static final String ARTIFACTID_PARAM = "artifactId";
	private static final String VERSION_PARAM = "version";
	private static final String PACKAGING_PARAM = "packaging";
	private static final String CLASSIFIER_PARAM = "classifier";

	private static final String DEF_REPO_URL = "http://repository.marvelution.com";
	private static final String DEF_REPO = "releases";

	private final JsonParser jsonParser = new JsonParser();

	private AsyncHttpClient asyncHttpClient;

	private String jsonString;
	
	/**
	 * Constructor
	 *
	 * @param macroAssistant the {@link MacroAssistant} implementation
	 * @param linkAssistant the {@link LinkAssistant} implementation
	 * @param velocityHelper the {@link VelocityHelperService} implementation
	 */
	public MavenArtifactInfoMacro(MacroAssistant macroAssistant, LinkAssistant linkAssistant,
			VelocityHelperService velocityHelper) {
		super(macroAssistant, linkAssistant, velocityHelper);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String execute(MacroInfo info) throws MacroExecutionException {
		String body = super.execute(info);
		return StringUtils.replace(body, "%JSON%", jsonString);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void populateContext(Map<String, Object> context, MacroInfo info) throws MacroExecutionException {
		IndexedStorage params = info.getMacroParams();
		String repoUrl = params.getString(REPO_URL_PARAM, DEF_REPO_URL);
		String repo = params.getString(REPO_PARAM, DEF_REPO);
		String groupId = params.getString(GROUPID_PARAM, null);
		String artifactId = params.getString(ARTIFACTID_PARAM, null);
		JsonObject configuration = new JsonObject();
		configuration.addProperty("repositoryURL", repoUrl);
		configuration.addProperty("groupId", groupId);
		configuration.addProperty("artifactId", artifactId);
		if (StringUtils.isBlank(groupId) || StringUtils.isBlank(artifactId)) {
			throw new MacroExecutionException("Please supply the groupId and artifactId");
		}
		Map<String, String> searchParams = getNexusSearchParameters(params);
		try {
			JsonObject json = getAsyncHttpClient().prepareGet(repoUrl + getNexusLuceneSearchURL(searchParams))
					.addHeader("Accept", "application/json").execute(new JsonObjectCompletionHandler()).get();
			if (!json.has("repoDetails") || json.getAsJsonArray("data").size() == 0) {
				throw new MacroExecutionException("No Maven artifacts found to display.");
			}
			JsonObject repository = null;
			for (JsonElement element : json.getAsJsonArray("repoDetails")) {
				if (repo.equals(element.getAsJsonObject().getAsJsonPrimitive("repositoryId").getAsString()) ){
					repository = element.getAsJsonObject();
					break;
				}
			}
			if (repository == null) {
				throw new MacroExecutionException("No Repository with Id " + repo);
			}
			configuration.add("repository", repository);
			JsonArray artifactVersions = new JsonArray();
			final List<ArtifactVersion> versions = Lists.newArrayList();
			for (JsonElement dataElement : json.getAsJsonArray("data")) {
				final JsonObject artifact = dataElement.getAsJsonObject();
				for (JsonElement artifactHit : Iterables.filter(artifact.getAsJsonArray("artifactHits"),
						getRepositoryPredicate(repository))) {
					for (final JsonElement artifactLink : Iterables.filter(artifactHit.getAsJsonObject()
							.getAsJsonArray("artifactLinks"), getArtifactLinkPredicates(params))) {
						artifactLink.getAsJsonObject().add("version", artifact.getAsJsonPrimitive("version"));
						artifactVersions.add(artifactLink);
						versions.add(new ArtifactVersion().setVersion(artifact.getAsJsonPrimitive("version").getAsString())
							.setPackaging(artifactLink.getAsJsonObject().getAsJsonPrimitive("extension").getAsString()));
					}
				}
			}
			configuration.add("artifacts", artifactVersions);
			Ordering<ArtifactVersion> ordering = Ordering.natural().reverse().onResultOf(
				new Function<ArtifactVersion, String>() {
					@Override
					public String apply(ArtifactVersion from) {
						return from.getName();
					}
			});
			context.put("versions", ordering.sortedCopy(versions));
			jsonString = new GsonBuilder().disableHtmlEscaping().create().toJson(configuration);
		} catch (IOException e) {
			LOGGER.error("Failed to connect to the Repoistory: " + e.getMessage(), e);
			throw new MacroExecutionException("Unable to connect to Repository: " + repoUrl);
		} catch (InterruptedException e) {
			LOGGER.error("Unable to parse the server response: " + e.getMessage(), e);
			throw new MacroExecutionException("Unable to parse the server response");
		} catch (ExecutionException e) {
			LOGGER.error("Unable to parse the server response: " + e.getMessage(), e);
			throw new MacroExecutionException("Unable to parse the server response");
		}
	}

	/**
	 * Getter for the {@link #asyncHttpClient}
	 * 
	 * @return the {@link AsyncHttpClient}
	 */
	/*package*/ AsyncHttpClient getAsyncHttpClient() {
		if (asyncHttpClient == null) {
			asyncHttpClient = new AsyncHttpClient();
		}
		return asyncHttpClient;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void finalize() throws Throwable {
		if (getAsyncHttpClient() != null && !getAsyncHttpClient().isClosed()) {
			getAsyncHttpClient().close();
		}
		super.finalize();
	}

	/**
	 * Get the {@link Map} of parameters to use to search in Nexus
	 * 
	 * @param info
	 * @return the {@link Map} of parameters
	 */
	private Map<String, String> getNexusSearchParameters(IndexedStorage parameters) {
		Map<String, String> params = Maps.newHashMap();
		params.put("g", parameters.getString(GROUPID_PARAM, ""));
		params.put("a", parameters.getString(ARTIFACTID_PARAM, ""));
		if (parameters.getString(VERSION_PARAM, null) != null) {
			params.put("v", parameters.getString(VERSION_PARAM, ""));
		}
		if (parameters.getString(PACKAGING_PARAM, null) != null) {
			params.put("p", parameters.getString(PACKAGING_PARAM, ""));
		}
		if (parameters.getString(CLASSIFIER_PARAM, null) != null) {
			params.put("c", parameters.getString(CLASSIFIER_PARAM, ""));
		}
		return params;
	}

	/**
	 * Get the Lucene Search URL
	 * 
	 * @param searchParams the parameters for the search
	 * @return the search REST URL
	 */
	private String getNexusLuceneSearchURL(Map<String, String> searchParams) {
		StringBuilder url = new StringBuilder("/service/local/lucene/search?");
		for (Entry<String, String> entry : searchParams.entrySet()) {
			url.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		return url.toString();
	}

	/**
	 * Get a {@link Predicate} for to match {@link JsonElement}s against the Repository {@link JsonObject} given
	 * 
	 * @param repository the Repository {@link JsonObject}
	 * @return the {@link Predicate}
	 */
	private Predicate<JsonElement> getRepositoryPredicate(final JsonObject repository) {
		return new Predicate<JsonElement>() {
			@Override
			public boolean apply(JsonElement input) {
				return repository.getAsJsonPrimitive("repositoryId").equals(
					input.getAsJsonObject().getAsJsonPrimitive("repositoryId"));
			}
		};
	}

	/**
	 * Get the Artifact Link {@link Predicate}
	 * 
	 * @param parameters the Macro parameters to build the Artifact Links Predicates
	 * @return the {@link Predicate}
	 */
	private Predicate<JsonElement> getArtifactLinkPredicates(IndexedStorage parameters) {
		List<Predicate<JsonElement>> predicates = Lists.newArrayList();
		final String packaging = parameters.getString(PACKAGING_PARAM, null);
		final String classifier = parameters.getString(CLASSIFIER_PARAM, null);
		// Predicate to ignore POM artifacts
		predicates.add(new Predicate<JsonElement>() {
			@Override
			public boolean apply(JsonElement input) {
				return !"pom".equals(input.getAsJsonObject().getAsJsonPrimitive("extension").getAsString());
			}
		});
		// Add the packaging predicate if needed
		if (StringUtils.isNotBlank(packaging)) {
			predicates.add(new Predicate<JsonElement>() {
				@Override
				public boolean apply(JsonElement input) {
					return packaging.equals(input.getAsJsonObject().getAsJsonPrimitive("extension").getAsString());
				}
			});
		}
		// Add the classifier predicate if needed
		if (StringUtils.isNotBlank(classifier)) {
			predicates.add(new Predicate<JsonElement>() {
				@Override
				public boolean apply(JsonElement input) {
					return (input.getAsJsonObject().has("classifier") && classifier.equals(
						input.getAsJsonObject().getAsJsonPrimitive("classifier").getAsString()));
				}
			});
		} else {
			predicates.add(new Predicate<JsonElement>() {
				@Override
				public boolean apply(JsonElement input) {
					return !input.getAsJsonObject().has("classifier");
				}
			});
		}
		return Predicates.and(predicates);
	}

	/**
	 * Custom {@link AsyncCompletionHandler} for {@link JsonObject} responses
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 *
	 * @since 1.0.0
	 */
	protected class JsonObjectCompletionHandler extends AsyncCompletionHandler<JsonObject> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public JsonObject onCompleted(Response response) throws Exception {
			InputStreamReader reader = new InputStreamReader(response.getResponseBodyAsStream());
			try {
				return (JsonObject) jsonParser.parse(reader);
			} finally {
				IOUtils.closeQuietly(reader);
			}
		}

	}

}
