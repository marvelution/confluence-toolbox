/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;

/**
 * Space Listing Macro
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 1.0.0
 */
public class SpaceListByLabelMacro extends BaseDirectoryListMacro {

	private static final String LABEL_PARAM = "label";
	private static final String SIMPLE_PARAM = "simple";

	/**
	 * Constructor
	 *
	 * @param macroAssistant the {@link MacroAssistant} implementation
	 * @param linkAssistant the {@link LinkAssistant} implementation
	 * @param velocityHelper the {@link VelocityHelperService} implementation
	 * @param labelManager the {@link LabelManager} implementation
	 * @param pageManager the {@link PageManager} implementation
	 */
	public SpaceListByLabelMacro(MacroAssistant macroAssistant, LinkAssistant linkAssistant,
			VelocityHelperService velocityHelper, LabelManager labelManager, PageManager pageManager) {
		super(macroAssistant, linkAssistant, velocityHelper, labelManager, pageManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void populateContext(Map<String, Object> context, MacroInfo info) throws MacroExecutionException {
		String labelName = info.getMacroParams().getString(LABEL_PARAM, null);
		if (StringUtils.isBlank(labelName)) {
			throw new MacroExecutionException("Please provide a Label name");
		}
		Label label = getLabel(labelName);
		if (label != null) {
			context.put("simpleList", info.getMacroParams().getBoolean(SIMPLE_PARAM, false));
			context.put("spaces", getSpacesWithLabel(label));
		} else {
			throw new MacroExecutionException("No label found with name: " + label);
		}
	}

}
