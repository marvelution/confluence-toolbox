/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.randombits.support.confluence.ConfluenceMacro;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.util.GeneralUtil;
import com.google.common.collect.Maps;
import com.marvelution.confluence.plugins.toolbox.utils.PluginHelper;

/**
 * Base Velocity Macro
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public abstract class BaseVelocityMacro extends ConfluenceMacro {

	private LinkAssistant linkAssistant;
	private VelocityHelperService velocityHelper;

	/**
	 * Constructor
	 *
	 * @param macroAssistant the {@link MacroAssistant} implementation
	 * @param linkAssistant the {@link LinkAssistant} implementation
	 * @param velocityHelper the {@link VelocityHelperService} implementation
	 */
	public BaseVelocityMacro(MacroAssistant macroAssistant, LinkAssistant linkAssistant,
			VelocityHelperService velocityHelper) {
		super(macroAssistant);
		this.linkAssistant = linkAssistant;
		this.velocityHelper = velocityHelper;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String execute(MacroInfo info) throws MacroExecutionException {
		Map<String, Object> context = Maps.newHashMap();
		context.putAll(velocityHelper.createDefaultVelocityContext());
		context.put("baseUrl", GeneralUtil.getGlobalSettings().getBaseUrl());
		context.put("contextPath", linkAssistant.getContextPath());
		context.put("space", info.getSpace());
		context.put("page", info.getContent());
		context.put("body", info.getMacroBody());
		context.put("stringUtils", new StringUtils());
		context.putAll(PluginHelper.getPluginPropertiesAsMap());
		context.putAll(info.getMacroParamsMap());
		populateContext(context, info);
		return velocityHelper.getRenderedTemplate(getTemplate(), context);
	}

	/**
	 * Populate the Velocity Parameters {@link Map}
	 * 
	 * @param context the default context {@link Map}
	 * @param info the {@link MacroInfo}
	 * @throws MacroExecutionException in case of errors
	 */
	protected abstract void populateContext(Map<String, Object> context, MacroInfo info) throws MacroExecutionException;

	/**
	 * Getter for the template name
	 * 
	 * @return the template name
	 */
	protected String getTemplate() {
		return "com/marvelution/confluence/plugins/toolbox/macros/templates/" + getClass().getSimpleName() + ".vm";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

}
