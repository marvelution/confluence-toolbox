/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import org.apache.log4j.Logger;
import org.randombits.storage.IndexedStorage;
import org.randombits.support.confluence.ConfluenceMacro;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;

import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.model.links.DefaultLink;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.PageResourceIdentifier;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.xhtml.api.Link;
import com.atlassian.confluence.xhtml.api.PlainTextLinkBody;

/**
 * Directory Link Macro
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class DirectoryLinkMacro extends ConfluenceMacro {

	private static final Logger LOGGER = Logger.getLogger(DirectoryLinkMacro.class);

	/**
	 * Constructor
	 *
	 * @param macroAssistant
	 * @param viewPageLinkMarshaller
	 */
	public DirectoryLinkMacro(MacroAssistant macroAssistant) {
		super(macroAssistant);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String execute(MacroInfo info) throws MacroExecutionException {
		Space space = info.getSpace();
		IndexedStorage parameters = info.getMacroParams();
		Type type = Type.fromValue(parameters.getString("type", Type.DOCUMENTATION.name()));
		String text = parameters.getString("text", space.getName());
		Link link = new DefaultLink(new PageResourceIdentifier(type.space, space.getName()),
			new PlainTextLinkBody(text));
		try {
			return getXhtmlContent().convertLinkToView(link, info.getConversionContext());
		} catch (XhtmlException e) {
			throw new MacroExecutionException(e.getMessage(), e);
		}
	}

	/**
	 * {@link Type} Enum
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 *
	 * @since 1.0.0
	 */
	/*package*/ enum Type {

		REPOSITORY("REPO"), DOCUMENTATION("ALLDOCS");

		public final String space;

		/**
		 * Constructor
		 *
		 * @param space the space name
		 */
		private Type(String space) {
			this.space = space;
		}

		/**
		 * Get a {@link Type} for the given {@link String}
		 * 
		 * @param string the {@link String} to parse
		 * @return the {@link Type}
		 */
		public static Type fromValue(String string) {
			LOGGER.debug("Parsing '" + string + "' to a DirectoryLinkMacro.Type");
			return Type.valueOf(string.toUpperCase());
		}

	}

}
