/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import org.randombits.support.confluence.ConfluenceMacro;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;

import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.model.links.DefaultLink;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.SpaceResourceIdentifier;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.spaces.Space;

/**
 * Macro implementation to display the space name
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class SpaceNameMacro extends ConfluenceMacro {

	/**
	 * Constructor
	 *
	 * @param macroAssistant the {@link MacroAssistant} implementation
	 */
	public SpaceNameMacro(MacroAssistant macroAssistant) {
		super(macroAssistant);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String execute(MacroInfo info) throws MacroExecutionException {
		Space space = info.getSpace();
		if (info.getMacroParams().getBoolean("as-link", false)) {
			try {
				return getXhtmlContent().convertLinkToView(
					new DefaultLink(new SpaceResourceIdentifier(space.getKey()), null), info.getConversionContext());
			} catch (XhtmlException e) {
				throw new MacroExecutionException(e.getMessage(), e);
			}
		} else {
			return space.getName();
		}
	}

}
