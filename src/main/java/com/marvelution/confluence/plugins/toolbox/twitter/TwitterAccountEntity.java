/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.twitter;

import twitter4j.User;
import twitter4j.auth.AccessToken;

/**
 * The {@link TwitterAccount} entity implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class TwitterAccountEntity {

	private TwitterAccount account;
	private User twitterProfile;

	/**
	 * Constructor
	 *
	 * @param account the {@link TwitterAccount}
	 */
	public TwitterAccountEntity(TwitterAccount account) {
		this.account = account;
	}

	/**
	 * Getter of the Twitter {@link User}
	 * 
	 * @return the Twitter {@link User}
	 * @see com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccount#getTwitterProfile()
	 */
	public User getTwitterProfile() {
		return twitterProfile;
	}

	/**
	 * Setter of the Twitter {@link User}
	 * 
	 * @param twitterProfile the Twitter {@link User}
	 * @see com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccount#setTwitterProfile(twitter4j.User)
	 */
	public void setTwitterProfile(User twitterProfile) {
		this.twitterProfile = twitterProfile;
	}

	/**
	 * Getter for the Twitter {@link AccessToken}
	 * 
	 * @return the {@link AccessToken}
	 * @see com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccount#getAccessToken()
	 */
	public AccessToken getAccessToken() {
		return new AccessToken(account.getToken(), account.getTokenSecret());
	}

}
