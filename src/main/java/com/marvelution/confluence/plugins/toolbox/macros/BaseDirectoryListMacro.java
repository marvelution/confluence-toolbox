/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import java.util.List;

import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroAssistant;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.Space;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;

/**
 * Base Directory Listing Macro
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 1.0.0
 */
public abstract class BaseDirectoryListMacro extends BaseVelocityMacro {

	protected static final String TITLE_PARAM = "title";
	protected static final String PROJECT_LABEL_NAME = "project";
	protected final LabelManager labelManager;
	protected final PageManager pageManager;

	/**
	 * Constructor
	 *
	 * @param macroAssistant the {@link MacroAssistant} implementation
	 * @param linkAssistant the {@link LinkAssistant} implementation
	 * @param velocityHelper the {@link VelocityHelperService} implementation
	 * @param labelManager the {@link LabelManager} implementation
	 * @param pageManager the {@link PageManager} implementation
	 */
	public BaseDirectoryListMacro(MacroAssistant macroAssistant, LinkAssistant linkAssistant,
			VelocityHelperService velocityHelper, LabelManager labelManager, PageManager pageManager) {
		super(macroAssistant, linkAssistant, velocityHelper);
		this.labelManager = labelManager;
		this.pageManager = pageManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	/**
	 * Getter for the "project" {@link Label}
	 * 
	 * @return the "project" {@link Label}
	 */
	protected Label getLabel() {
		return getLabel(PROJECT_LABEL_NAME);
	}

	/**
	 * Getter for the {@link Label} with the given name
	 * 
	 * @param labelName the name of the {@link Label} to get
	 * @return the {@link Label}
	 */
	protected Label getLabel(String labelName) {
		return labelManager.getLabel(labelName);
	}

	/**
	 * Getter of the Ordered {@link Space} {@link List} that have the given {@link Label}
	 * 
	 * @param label the {@link Label} to get he Spaces by
	 * @return the Ordered {@link List} of {@link Space} objects
	 */
	protected List<Space> getSpacesWithLabel(Label label) {
		Ordering<Space> ordering = Ordering.natural().onResultOf(new Function<Space, String>() {
			@Override
			public String apply(Space from) {
				return from.getName();
			}
		});
		return ordering.sortedCopy(labelManager.getSpacesWithLabel(label));
	}

}
