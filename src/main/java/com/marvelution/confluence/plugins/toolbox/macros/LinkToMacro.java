/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;

import org.randombits.storage.IndexedStorage;
import org.randombits.support.confluence.ConfluenceMacro;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.thoughtworks.xstream.XStream;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@SuppressWarnings("unchecked")
public class LinkToMacro extends ConfluenceMacro {

	private static final String SPACE_PARAM = "space";
	private static final String TEXT_PARAM = "text";

	private static final String SPACEKEY_MARKER = "%spacekey%";

	private static Map<String, String> locations;

	private final LinkAssistant linkAssistant;

	/**
	 * Constructor
	 *
	 * @param macroAssistant the {@link MacroAssistant} implementation
	 * @param linkAssistant the {@link LinkAssistant} implementation
	 */
	public LinkToMacro(MacroAssistant macroAssistant, LinkAssistant  linkAssistant) {
		super(macroAssistant);
		this.linkAssistant = linkAssistant;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String execute(MacroInfo info) throws MacroExecutionException {
		IndexedStorage parameters = info.getMacroParams();
		String spaceKey = parameters.getString(SPACE_PARAM, info.getSpace().getKey());
		String target = parameters.getString(0, null);
		String text = parameters.getString(TEXT_PARAM, target);
		if (locations.containsKey(target)) {
			String url = locations.get(target);
			if (url.contains(SPACEKEY_MARKER)) {
				url = url.replaceAll(SPACEKEY_MARKER, spaceKey);
			}
			StringBuilder link = new StringBuilder();
			link.append("<a href=\"").append(linkAssistant.getContextPath()).append(url).append("\">");
			link.append(text).append("</a>");
			return link.toString();
		}
		throw new MacroExecutionException("Please supply the target.");
	}

	static {
		XStream xStream = new XStream();
	    Reader in = new InputStreamReader(LinkToMacro.class.getResourceAsStream("locations.xml"));
		locations = (Map<String, String>)xStream.fromXML(in);
	}

}
