/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.rest;

import java.io.InputStreamReader;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.atlassian.user.User;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.marvelution.confluence.plugins.toolbox.macros.maven.ArtifactVersion;
import com.marvelution.confluence.plugins.toolbox.rest.exceptions.MavenArtifactDescriberException;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.sun.jersey.spi.resource.Singleton;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@Path("/maven")
@AnonymousAllowed
@Singleton
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class MavenArtifactDescriber {

	private static final Logger LOGGER = Logger.getLogger(MavenArtifactDescriber.class);

	private final UserAccessor userAccessor;
	private final FormatSettingsManager formatSettingsManager;
	private final LocaleManager localeManager;

	private final JsonParser jsonParser = new JsonParser();

	private AsyncHttpClient asyncHttpClient;

	/**
	 * Constructor
	 *
	 * @param userAccessor the {@link UserAccessor} implementation
	 * @param formatSettingsManager the {@link FormatSettingsManager} implementation
	 * @param localeManager the {@link LocaleManager} implementation
	 */
	public MavenArtifactDescriber(UserAccessor userAccessor, FormatSettingsManager formatSettingsManager,
			LocaleManager localeManager) {
		this.userAccessor = userAccessor;
		this.formatSettingsManager = formatSettingsManager;
		this.localeManager = localeManager;
	}

	@GET
	@Path("/describe/{groupId}/{artifactId}/{version}/{extension}")
	public ArtifactVersion describeMavenArtifact(@PathParam("groupId") String groupId,
			@PathParam("artifactId") String artifactId, @PathParam("version") String version,
			@PathParam("extension") String extension, @QueryParam("classifier") String classifier,
			@QueryParam("repositoryId") String repositoryId, @QueryParam("repositoryURL") String repositoryURL,
			@Context AuthenticationContext authenticationContext) {
		StringBuilder url = new StringBuilder(repositoryURL).append("/service/local/repositories/");
		url.append(repositoryId).append("/content/").append(groupId.replace('.', '/')).append("/").append(artifactId);
		url.append("/").append(version).append("/").append(artifactId).append("-").append(version);
		if (StringUtils.isNotBlank(classifier)) {
			url.append("-").append(classifier);
		}
		url.append(".").append(extension).append("?describe=info&isLocal=true");
		try {
			ArtifactVersion artifactVersion = getAsyncHttpClient().prepareGet(url.toString())
				.addHeader("Accept", "application/json")
				.execute(new ArtifactVersionCompletionHandler(getDateFormatter(authenticationContext))).get();
			return artifactVersion.setVersion(version).setPackaging(extension);
		} catch (Exception e) {
			LOGGER.error("Failed to get the descriptive information", e);
			throw new MavenArtifactDescriberException("Failed to get the descriptive information of artifact "
				+ groupId + ":" + artifactId + ":" + version + ":" + extension);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void finalize() throws Throwable {
		if (getAsyncHttpClient() != null && !getAsyncHttpClient().isClosed()) {
			getAsyncHttpClient().close();
		}
		super.finalize();
	}

	/**
	 * Getter for the {@link #asyncHttpClient}
	 * 
	 * @return the {@link AsyncHttpClient}
	 */
	/*package*/ AsyncHttpClient getAsyncHttpClient() {
		if (asyncHttpClient == null) {
			asyncHttpClient = new AsyncHttpClient();
		}
		return asyncHttpClient;
	}

	/*package*/ DateFormatter getDateFormatter(AuthenticationContext authenticationContext) {
		if (authenticationContext.getPrincipal() != null) {
			// We have a use, get his/her DateFormatter
			User user = userAccessor.getUser(authenticationContext.getPrincipal().getName());
			return userAccessor.getConfluenceUserPreferences(user).getDateFormatter(formatSettingsManager,
				localeManager);
		} else {
			// Else create a default DateFormatter
			return new DateFormatter(TimeZone.getDefault(), formatSettingsManager, localeManager);
		}
	}

	/**
	 * Custom {@link AsyncCompletionHandler} for {@link ArtifactVersion} responses
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 *
	 * @since 1.0.0
	 */
	protected class ArtifactVersionCompletionHandler extends AsyncCompletionHandler<ArtifactVersion> {

		private DateFormatter dateFormatter;

		/**
		 * Constructor
		 *
		 * @param dateFormatter the {@link DateFormatter} implementation
		 */
		private ArtifactVersionCompletionHandler(DateFormatter dateFormatter) {
			this.dateFormatter = dateFormatter;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ArtifactVersion onCompleted(com.ning.http.client.Response response) throws Exception {
			InputStreamReader reader = new InputStreamReader(response.getResponseBodyAsStream());
			try {
				JsonObject json = jsonParser.parse(reader).getAsJsonObject()
					.getAsJsonObject("data");
				ArtifactVersion version = new ArtifactVersion();
				version.setMd5Hash(json.getAsJsonPrimitive("md5Hash").getAsString());
				version.setSha1Hash(json.getAsJsonPrimitive("sha1Hash").getAsString());
				version.setRepositoryId(json.getAsJsonPrimitive("repositoryId").getAsString());
				version.setRepositoryPath(json.getAsJsonPrimitive("repositoryPath").getAsString());
				version.setSize(FileUtils.byteCountToDisplaySize(json.getAsJsonPrimitive("size").getAsLong()));
				version.setUploaded(dateFormatter.format(new Date(json.getAsJsonPrimitive("uploaded").getAsLong())));
				return version;
			} finally {
				IOUtils.closeQuietly(reader);
			}
		}

	}

}
