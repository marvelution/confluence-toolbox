/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import org.apache.commons.lang.StringUtils;
import org.randombits.storage.IndexedStorage;
import org.randombits.support.confluence.ConfluenceMacro;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;

import com.atlassian.confluence.macro.MacroExecutionException;

/**
 * Mail-to Macro
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class MailToMacro extends ConfluenceMacro {

	/**
	 * Constructor
	 *
	 * @param macroAssistant
	 */
	public MailToMacro(MacroAssistant macroAssistant) {
		super(macroAssistant);
	}

	private static final String EMAIL_PARAM = "email";
	private static final String TEXT_PARAM = "text";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String execute(MacroInfo info) throws MacroExecutionException {
		StringBuilder out = new StringBuilder("<a href=\"mailto:");
		IndexedStorage params = info.getMacroParams();
		if (StringUtils.isBlank(params.getString(EMAIL_PARAM, null))) {
			throw new MacroExecutionException("Please supply an email address.");
		}
		out.append(params.getString(EMAIL_PARAM, null)).append("\">");
		out.append(params.getString(TEXT_PARAM, params.getString(EMAIL_PARAM, null)));
		return out.append("</a>").toString();
	}

}
