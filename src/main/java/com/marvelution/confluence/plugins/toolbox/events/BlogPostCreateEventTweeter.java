/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.events;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;

import twitter4j.Twitter;
import twitter4j.TwitterException;

import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.TinyUrl;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccount;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccountManager;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterClientFactory;

/**
 * {@link EventListener} to tweet when new Blog entries are posted
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class BlogPostCreateEventTweeter implements DisposableBean {

	private static final Logger LOGGER = Logger.getLogger(BlogPostCreateEventTweeter.class);
	private static final int MAX_TWEET_LENGTH = 140;

	private final EventPublisher eventPublisher;
	private final TwitterClientFactory clientFactory;
	private final TwitterAccountManager accountService;

	/**
	 * Constructor
	 *
	 * @param eventPublisher the {@link EventPublisher} implementation
	 * @param clientFactory the {@link TwitterClientFactory} implementation
	 * @param accountService the {@link TwitterAccountManager} implementation
	 */
	public BlogPostCreateEventTweeter(EventPublisher eventPublisher, TwitterClientFactory clientFactory,
			TwitterAccountManager accountService) {
		this.eventPublisher = eventPublisher;
		this.clientFactory = clientFactory;
		this.accountService = accountService;
		this.eventPublisher.register(this);
	}

	/**
	 * The Event Listener for the {@link BlogPostCreateEvent}
	 * 
	 * @param event the {@link BlogPostCreateEvent}
	 */
	@EventListener
	public void tweetBlogPost(BlogPostCreateEvent event) {
		for (TwitterAccount account : accountService.find(event.getBlogPost().getCreatorName())) {
			if (account.isAutoTweetBlogPosts()) {
				Twitter twitter = clientFactory.create(account);
				String tweet = event.getBlogPost().getTitle();
				String url = getTinyUrl(event.getBlogPost());
				if (tweet.length() + url.length() > MAX_TWEET_LENGTH - 1) {
					tweet = tweet.substring(0, MAX_TWEET_LENGTH - url.length() - 4) + "...";
				}
				try {
					twitter.updateStatus(tweet + " " + url);
				} catch (TwitterException e) {
					LOGGER.error("Failed to tweet the new blog entry: " + e.getErrorMessage());
					LOGGER.debug("Tweet failure trace", e);
				}
			}
		}
	}

	/**
	 * Getter for the Blog Post Page tiny URL
	 * 
	 * @param blogPost the {@link BlogPost} to get hte tiny url for
	 * @return the tiny url
	 */
	protected String getTinyUrl(BlogPost blogPost) {
		TinyUrl tinyUrl = new TinyUrl(blogPost);
		return GeneralUtil.getGlobalSettings().getBaseUrl() + "/x/" + tinyUrl.getIdentifier();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() throws Exception {
		eventPublisher.unregister(this);
	}

}
