/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.twitter.action;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import twitter4j.Twitter;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import com.atlassian.confluence.user.actions.AbstractUserProfileAction;
import com.google.common.collect.Lists;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccount;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterAccountManager;
import com.marvelution.confluence.plugins.toolbox.twitter.TwitterClientFactory;

/**
 * Twitter User Profile Action
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class TwitterProfileAction extends AbstractUserProfileAction {

	private static final long serialVersionUID = 1L;
	private static final String CALLBACK_URL = "/users/connect.action";
	private static final String REQUEST_TOKEN_KEY = "request_token";
	private static final Logger LOGGER = Logger.getLogger(TwitterProfileAction.class);

	private TwitterAccountManager accountService;
	private TwitterClientFactory clientFactory;
	private RequestToken requestToken;
	private List<TwitterAccount> accounts;
	private String oauth_token;
	private String oauth_verifier;
	private int accountId = 0;

	/**
	 * XWork method to display the twitter user profile page
	 * 
	 * @return the xwork view name
	 * @throws Exception in case of errors
	 */
	@SuppressWarnings("unchecked")
	public String twitter() throws Exception {
		Twitter twitter = clientFactory.create();
		requestToken = twitter.getOAuthRequestToken(settingsManager.getGlobalSettings().getBaseUrl() + CALLBACK_URL);
		getSession().put(REQUEST_TOKEN_KEY, requestToken);
		accounts = Lists.newArrayList();
		for (TwitterAccount account : accountService.find(getUsername())) {
			try {
				twitter = clientFactory.create(account);
				account.setTwitterProfile(twitter.showUser(twitter.getId()));
				accounts.add(account);
			} catch (Exception e) {
				LOGGER.error("Failed to get the Twitter User Profile", e);
			}
		}
		return INPUT;
	}

	/**
	 * XWork method to connect a new {@link TwitterAccount}
	 * 
	 * @return the xwork view name
	 * @throws Exception in case of errors
	 */
	public String connect() throws Exception {
		if (StringUtils.isNotBlank(oauth_token) && StringUtils.isNotBlank(oauth_verifier)) {
			RequestToken requestToken = ((RequestToken) getSession().get(REQUEST_TOKEN_KEY));
			if (requestToken != null) {
				AccessToken token = clientFactory.create().getOAuthAccessToken(requestToken, oauth_verifier);
				accountService.add(getUsername(), token);
			}
		}
		return SUCCESS;
	}

	/**
	 * XWork method to disconnect a {@link TwitterAccount}
	 * 
	 * @return the xwork view name
	 * @throws Exception in case of errors
	 */
	public String disconnect() throws Exception {
		TwitterAccount account = accountService.get(accountId);
		if (account != null) {
			accountService.destroy(account);
		}
		return SUCCESS;
	}

	/**
	 * XWork method to toggle the Auto Tweet Blog Post of a {@link TwitterAccount}
	 * 
	 * @return the xwork view name
	 * @throws Exception in case of errors
	 */
	public String toggleAutoBlogPost() throws Exception {
		TwitterAccount account = accountService.get(accountId);
		if (account != null) {
			if (account.isAutoTweetBlogPosts()) {
				accountService.disableAutoTweetBlogPosts(account);
			} else {
				accountService.enableAutoTweetBlogPosts(account);
			}
		}
		return SUCCESS;
	}

	/**
	 * Get all the {@link TwitterAccount}s configured for this user
	 * 
	 * @return {@link List} of all {@link TwitterAccount}s
	 */
	public List<TwitterAccount> getAccounts() {
		return accounts;
	}

	/**
	 * Getter for the Twitter Authorization URL
	 * 
	 * @return the Authorization URL
	 */
	public String getAuthorizationURL() {
		return requestToken.getAuthorizationURL();
	}

	/**
	 * Setter for accountId
	 *
	 * @param accountId the accountId to set
	 */
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	/**
	 * Setter for {@link #oauth_token}
	 * 
	 * @param oauth_token the oauth_token to set
	 */
	public void setOauth_token(String oauth_token) {
		this.oauth_token = oauth_token;
	}

	/**
	 * Setter for the {@link #oauth_verifier}
	 * 
	 * @param oauth_verifier the oauth_verifier to set
	 */
	public void setOauth_verifier(String oauth_verifier) {
		this.oauth_verifier = oauth_verifier;
	}

	/**
	 * Setter for accountService
	 *
	 * @param accountService the accountService to set
	 */
	public void setAccountService(TwitterAccountManager accountService) {
		this.accountService = accountService;
	}

	/**
	 * Setter for clientFactory
	 *
	 * @param clientFactory the clientFactory to set
	 */
	public void setClientFactory(TwitterClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

}
