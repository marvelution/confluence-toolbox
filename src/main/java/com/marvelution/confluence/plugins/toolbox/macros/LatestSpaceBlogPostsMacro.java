/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros;

import java.util.Map;

import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.confluence.MacroInfo;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;

/**
 * Documentation Directory Listing Macro
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 1.0.0
 */
public class LatestSpaceBlogPostsMacro extends BaseVelocityMacro {

	private final SpaceManager spaceManager;
	private final PageManager pageManager;

	/**
	 * Constructor
	 *
	 * @param macroAssistant the {@link MacroAssistant} implementation
	 * @param linkAssistant the {@link LinkAssistant} implementation
	 * @param velocityHelper the {@link VelocityHelperService} implementation
	 * @param spaceManager the {@link SpaceManager} implementation
	 * @param pageManager the {@link PageManager} implementation
	 */
	public LatestSpaceBlogPostsMacro(MacroAssistant macroAssistant, LinkAssistant linkAssistant,
			VelocityHelperService velocityHelper, SpaceManager spaceManager, PageManager pageManager) {
		super(macroAssistant, linkAssistant, velocityHelper);
		this.spaceManager = spaceManager;
		this.pageManager = pageManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void populateContext(Map<String, Object> context, MacroInfo info) throws MacroExecutionException {
		String spaceKey = info.getMacroParams().getString("space", info.getSpace().getKey());
		Space space = spaceManager.getSpace(spaceKey);
		int maxPosts = info.getMacroParams().getInteger("posts", 5);
		if (space != null) {
			context.put("blogPosts", pageManager.getRecentlyAddedBlogPosts(maxPosts, space.getKey()));
		} else {
			throw new MacroExecutionException("No space found with key: " + spaceKey);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}


}
