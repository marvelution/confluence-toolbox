/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.twitter;

import net.java.ao.DBParam;
import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;

import twitter4j.auth.AccessToken;

/**
 * Default {@link TwitterAccountManager} implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class TwitterAccountManagerService implements TwitterAccountManager {

	private final ActiveObjects objects;

	/**
	 * Constructor
	 *
	 * @param objects the {@link ActiveObjects} implementation
	 */
	public TwitterAccountManagerService(ActiveObjects objects) {
		this.objects = objects;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TwitterAccount add(String username, AccessToken token) {
		return add(username, token, false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TwitterAccount add(String username, AccessToken token, boolean autoTweetBlogPosts) {
		TwitterAccount account = objects.create(TwitterAccount.class, new DBParam("USERNAME", username),
			new DBParam("TOKEN", token.getToken()), new DBParam("TOKEN_SECRET", token.getTokenSecret()));
		account.setAutoTweetBlogPosts(autoTweetBlogPosts);
		account.save();
		return account;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TwitterAccount[] findAll() {
		return objects.find(TwitterAccount.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TwitterAccount[] find(String username) {
		return objects.find(TwitterAccount.class, Query.select().where("USERNAME = ?", username));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TwitterAccount get(int accountId) {
		return objects.get(TwitterAccount.class, accountId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy(TwitterAccount... account) {
		objects.delete(account);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enableAutoTweetBlogPosts(TwitterAccount account) {
		account.setAutoTweetBlogPosts(true);
		account.save();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void disableAutoTweetBlogPosts(TwitterAccount account) {
		account.setAutoTweetBlogPosts(false);
		account.save();
	}

}
