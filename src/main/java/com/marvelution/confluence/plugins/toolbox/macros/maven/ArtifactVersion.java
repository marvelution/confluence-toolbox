/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.macros.maven;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ArtifactVersion {

	private String version;
	private String uploaded;
	private String md5Hash;
	private String sha1Hash;
	private String repositoryId;
	private String repositoryPath;
	private String size;
	private String packaging;
	private String classifier;

	/**
	 * Getter for the id
	 * 
	 * @return the id
	 */
	@XmlElement(name = "id")
	public String getId() {
		return StringUtils.replaceChars(String.format("%1$s-%2$s", getVersion(), getPackaging().toLowerCase()), ".", "");
	}

	/**
	 * Getter for name
	 * 
	 * @return the name
	 */
	@XmlElement(name = "name")
	public String getName() {
		return String.format("%1$s - %2$s", getVersion(), getPackaging().toUpperCase());
	}

	/**
	 * Getter for version
	 *
	 * @return the version
	 */
	@XmlElement(name = "version")
	public String getVersion() {
		return version;
	}

	/**
	 * Setter for version
	 *
	 * @param version the version to set
	 * @return {@link ArtifactVersion}
	 */
	public ArtifactVersion setVersion(String version) {
		this.version = version;
		return this;
	}

	/**
	 * Getter for uploaded
	 * 
	 * @return the uploaded
	 */
	@XmlElement(name = "uploaded")
	public String getUploaded() {
		return uploaded;
	}

	/**
	 * Setter for uploaded
	 * 
	 * @param uploaded the uploaded to set
	 * @return {@link ArtifactVersion}
	 */
	public ArtifactVersion setUploaded(String uploaded) {
		this.uploaded = uploaded;
		return this;
	}

	/**
	 * Getter for md5Hash
	 * 
	 * @return the md5Hash
	 */
	@XmlElement(name = "md5Hash")
	public String getMd5Hash() {
		return md5Hash;
	}

	/**
	 * Setter for md5Hash
	 * 
	 * @param md5Hash the md5Hash to set
	 * @return {@link ArtifactVersion}
	 */
	public ArtifactVersion setMd5Hash(String md5Hash) {
		this.md5Hash = md5Hash;
		return this;
	}

	/**
	 * Getter for sha1Hash
	 * 
	 * @return the sha1Hash
	 */
	@XmlElement(name = "sha1Hash")
	public String getSha1Hash() {
		return sha1Hash;
	}

	/**
	 * Setter for sha1Hash
	 * 
	 * @param sha1Hash the sha1Hash to set
	 * @return {@link ArtifactVersion}
	 */
	public ArtifactVersion setSha1Hash(String sha1Hash) {
		this.sha1Hash = sha1Hash;
		return this;
	}

	/**
	 * Getter for repositoryId
	 * 
	 * @return the repositoryId
	 */
	@XmlElement(name = "repositoryId")
	public String getRepositoryId() {
		return repositoryId;
	}

	/**
	 * Setter for repositoryId
	 * 
	 * @param repositoryId the repositoryId to set
	 * @return {@link ArtifactVersion}
	 */
	public ArtifactVersion setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
		return this;
	}

	/**
	 * Getter for repositoryPath
	 * 
	 * @return the repositoryPath
	 */
	@XmlElement(name = "repositoryPath")
	public String getRepositoryPath() {
		return repositoryPath;
	}

	/**
	 * Setter for repositoryPath
	 * 
	 * @param repositoryPath the repositoryPath to set
	 * @return {@link ArtifactVersion}
	 */
	public ArtifactVersion setRepositoryPath(String repositoryPath) {
		this.repositoryPath = repositoryPath;
		return this;
	}

	/**
	 * Getter for size
	 * 
	 * @return the size
	 */
	@XmlElement(name = "size")
	public String getSize() {
		return size;
	}

	/**
	 * Setter for size
	 * 
	 * @param size the size to set
	 * @return {@link ArtifactVersion}
	 */
	public ArtifactVersion setSize(String size) {
		this.size = size;
		return this;
	}

	/**
	 * Getter for packaging
	 *
	 * @return the packaging
	 */
	@XmlElement(name = "packaging")
	public String getPackaging() {
		return packaging;
	}

	/**
	 * Setter for packaging
	 *
	 * @param packaging the packaging to set
	 * @return {@link ArtifactVersion}
	 */
	public ArtifactVersion setPackaging(String packaging) {
		this.packaging = packaging;
		return this;
	}

	/**
	 * Getter for classifier
	 *
	 * @return the classifier
	 */
	@XmlElement(name = "classifier")
	public String getClassifier() {
		return classifier;
	}

	/**
	 * Setter for classifier
	 *
	 * @param classifier the classifier to set
	 */
	public void setClassifier(String classifier) {
		this.classifier = classifier;
	}

}
