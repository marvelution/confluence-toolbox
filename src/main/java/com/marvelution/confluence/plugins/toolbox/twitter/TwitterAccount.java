/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.twitter;

import twitter4j.User;
import twitter4j.auth.AccessToken;
import net.java.ao.Entity;
import net.java.ao.Implementation;
import net.java.ao.Preload;
import net.java.ao.schema.Ignore;
import net.java.ao.schema.NotNull;

/**
 * Twitter Account {@link Entity}
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
@Preload
@Implementation(TwitterAccountEntity.class)
public interface TwitterAccount extends Entity {

	/**
	 * Getter of the username
	 * 
	 * @return the username
	 */
	@NotNull
	String getUsername();

	/**
	 * Setter for the username
	 * 
	 * @param username the username to set
	 */
	void setUsername(String username);

	/**
	 * Getter of the token
	 * 
	 * @return the token
	 */
	@NotNull
	String getToken();

	/**
	 * Setter for the Token
	 * 
	 * @param token the token to set
	 */
	void setToken(String token);

	/**
	 * Getter of the token secret
	 * 
	 * @return the token secret
	 */
	@NotNull
	String getTokenSecret();

	/**
	 * Setter for the token secret
	 * 
	 * @param tokenSecrect the token secret to set
	 */
	void setTokenSecret(String tokenSecrect);

	/**
	 * Getter of the auto tweet blog posts flag
	 * 
	 * @return <code>true</code> to auto tweet new blog posts, <code>false</code> otherwise
	 */
	boolean isAutoTweetBlogPosts();

	/**
	 * Setter for the auto tweet blog post flag
	 * 
	 * @param autoTweetBlogPosts <code>true</code> to auto tweet new blog posts, <code>false</code> otherwise
	 */
	void setAutoTweetBlogPosts(boolean autoTweetBlogPosts);

	/**
	 * Getter of the Twitter {@link User}
	 * 
	 * @return the Twitter {@link User}
	 */
	@Ignore
	User getTwitterProfile();

	/**
	 * Setter of the Twitter {@link User}
	 * 
	 * @param twitterProfile the Twitter {@link User}
	 */
	@Ignore
	void setTwitterProfile(User twitterProfile);

	/**
	 * Getter for the Twitter {@link AccessToken}
	 * 
	 * @return the {@link AccessToken}
	 */
	@Ignore
	AccessToken getAccessToken();

}
