/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.confluence.plugins.toolbox.twitter;

import twitter4j.auth.AccessToken;

/**
 * {@link TwitterAccount} service interface
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public interface TwitterAccountManager {

	/**
	 * Add a new {@link TwitterAccount}
	 *  
	 * @param username the Confluence username
	 * @param token the {@link AccessToken} to Twitter
	 * @return the newly created {@link TwitterAccount}
	 * @see #add(String, AccessToken, boolean)
	 */
	TwitterAccount add(String username, AccessToken token);

	/**
	 * Add a new {@link TwitterAccount}
	 *  
	 * @param username the Confluence username
	 * @param token the {@link AccessToken} to Twitter
	 * @param autoTweetBlogPosts <code>true</code> to auto tweet new blog posts, <code>false</code> otherwise
	 * @return the newly created {@link TwitterAccount}
	 */
	TwitterAccount add(String username, AccessToken token, boolean autoTweetBlogPosts);

	/**
	 * Get a {@link TwitterAccount} by Id
	 * 
	 * @param accountId the account Id to get
	 * @return the {@link TwitterAccount}, may be <code>null</code>
	 */
	TwitterAccount get(int accountId);

	/**
	 * Get all the configured {@link TwitterAccount}s
	 * 
	 * @return array of {@link TwitterAccount}s
	 */
	TwitterAccount[] findAll();

	/**
	 * Get all the configured {@link TwitterAccount}s for the given user
	 * 
	 * @param username the Confluence username to get all the {@link TwitterAccount}s for
	 * @return array of {@link TwitterAccount}s
	 */
	TwitterAccount[] find(String username);

	/**
	 * Destroy (Delete) the given {@link TwitterAccount}
	 * 
	 * @param account the {@link TwitterAccount}to destroy
	 */
	void destroy(TwitterAccount... account);

	/**
	 * Enable automatic tweeting of new blog posts for the given {@link TwitterAccount}
	 * 
	 * @param account the {@link TwitterAccount} to enable automatic blog tweeting for
	 */
	void enableAutoTweetBlogPosts(TwitterAccount account);

	/**
	 * Disable automatic tweeting of new blog posts for the given {@link TwitterAccount}
	 * 
	 * @param account the {@link TwitterAccount} to disable automatic blog tweeting for
	 */
	void disableAutoTweetBlogPosts(TwitterAccount account);

}
