/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Initiate the Maven info table
 * 
 * @param the Configuration object
 */
initiateMavenArtifactInformationTable = function(configuration) {
	AJS.$(configuration.artifacts).each(function(index, item) {
		AJS.$.ajax({
			type : "GET",
			async: true,
			dataType: "json",
			url : contextPath + "/rest/toolbox/1.0/maven/describe/" + configuration.groupId + "/"
				+ configuration.artifactId + "/" + item.version + "/" + item.extension,
			data: {
				classifier: item.classifier,
				repositoryId: configuration.repository.repositoryId,
				repositoryURL: configuration.repositoryURL
			},
			success: function(data) {
				var row = AJS.$("#" + data.id);
				var cells = row.find("td");
				AJS.$(cells[0]).text(data.size);
				AJS.$(cells[1]).text(data.uploaded);
				AJS.$(cells[2]).prepend(AJS.$("<a/>").attr({
					"href": evaluateMavenArtifactDownloadHref(configuration.repositoryURL,
							configuration.repository.repositoryId, data.repositoryPath)
				}).text(data.packaging));
				row.find("#md5").after(data.md5Hash);
				row.find("#sha1").after(data.sha1Hash);
				row.css("display", "");
				hideMavenArtifactTableLoadingRow(data);
			}
		});
	});
}

/**
 * Evaluate the given properties to an Nexus download link
 * 
 * @param repositoryURL the repo URL
 * @param repositoryId the repo id
 * @param repositoryPath the path of the artifact
 * @returns the URL
 */
evaluateMavenArtifactDownloadHref = function(repositoryURL, repositoryId, repositoryPath) {
	return repositoryURL + "/content/repositories/" + repositoryId + repositoryPath;
}

/**
 * Hide the loading thobber if the given loadedItem is the last item loaded
 * 
 * @param loadedItem the loaded item
 */
hideMavenArtifactTableLoadingRow = function(loadedItem) {
	var item = AJS.$("#" + loadedItem.id);
	if (item.parents("table:first").find("tr[style*='display: none;']").length == 0) {
		// No more loading versions, so hide the loading thobber
		item.parents("table:first").find("#thobber").addClass("done");
	}
}
